export default [{
  path: '/users',
  redirect: 'users-list'
}, {
  path: '/BeautyExperts/list',
  name: 'users-list',
  component: () => import(/* webpackChunkName: "users-list" */ '@/pages/BeautyExperts/UsersPage.vue')
},
{
  path: '/BeautyCenters/list',
  name: 'BeautyCenters-list',
  component: () => import(/* webpackChunkName: "users-list" */ '@/pages/BeautyExperts/BeautyCenters.vue')
}, {
  path: '/BeautyExperts/:id',
  name: 'BeautyExperts-Details',
  params:true,
  component: () => import(/* webpackChunkName: "users-edit" */ '@/pages/BeautyExperts/EditUserPage.vue')
}]
