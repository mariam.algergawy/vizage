import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

// Routes
import AppsRoutes from './apps.routes'
import UIRoutes from './ui.routes'
import PagesRoutes from './pages.routes'
import UsersRoutes from './BeautyExperts.routes'
import adminsRoutes from './users.routes'

import ProviderImagesRoutes from './ProviderImages.routes'
import notificationsRoutes from './notifications.routes'
import EcommerceRoutes from './ecommerce.routes'
import LandingRoutes from './landing.routes'
import ServicesRoutes from './services.routes'
Vue.use(Router)

export const routes = [{
  path: '/',
  redirect: '/dashboard/analytics'
}, {
  path: '/dashboard/analytics',
  name: 'dashboard-analytics',
  component: () => import(/* webpackChunkName: "dashboard" */ '@/pages/dashboard/DashboardPage.vue')
},
...AppsRoutes,
...UIRoutes,
...PagesRoutes,
...UsersRoutes,
...ProviderImagesRoutes,
...notificationsRoutes,
...EcommerceRoutes,
...LandingRoutes,
...ServicesRoutes,
...adminsRoutes,
{
  path: '/blank',
  name: 'blank',
  component: () => import(/* webpackChunkName: "blank" */ '@/pages/BlankPage.vue')
},
{
  path: '*',
  name: 'error',
  component: () => import(/* webpackChunkName: "error" */ '@/pages/error/NotFoundPage.vue'),
  meta: {
    layout: 'error'
  }
}]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL || '/',
  routes
})

/**
 * Before each route update
 */
router.beforeEach((to, from, next) => {
  if (to.path === '/auth/signin' && store.getters.isAuthenticated) {
    //if user is already logged in and tries to go to login
    return next('/') //redirect to home
  } else if (
    (to.path === '/auth/signin' || to.path === '/auth/signup') &&
    !store.getters.isAuthenticated
  ) {
    //if user is not logged in and tries to go to login or signup page
    return next()
  } else {
    //if user is logged in and tries to go to another page
    if (store.getters.isAuthenticated) {
      return next()
    } else {
      //if user is not logged in and tries to go to another page
      return next('/auth/signin')
    }
  }
})

export default router
