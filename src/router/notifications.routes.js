export default [{
  path: '/Notifications',
  redirect: 'push-Notifications'
},  {
  path: '/push/notifications',
  name: 'push-Notifications',
  component: () => import(/* webpackChunkName: "users-create" */ '@/pages/Notification/PushNotification.vue')
}
]
