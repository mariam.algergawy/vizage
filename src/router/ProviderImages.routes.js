export default [{
  path: '/providerImages',
  redirect: '/provider/Images'
}, {
  path: '/provider/Images',
  name: 'provider-Images',
  component: () => import(/* webpackChunkName: "utility-help" */ '@/pages/providerImages/ProviderImages.vue')
}
]
