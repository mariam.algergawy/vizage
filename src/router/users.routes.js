export default [
  {
    path: '/users',
    redirect: 'employees-list'
  },
  {
    path: '/employees/list',
    name: 'employees-list',
    component: () =>
      import(
        /* webpackChunkName: "users-list" */ '@/pages/employees/UsersPage.vue'
      )
  },
  {
    path: '/employees/edit/:singleemploye_id',
    name: 'employees-edit',
    props: true,
    component: () =>
      import(
        /* webpackChunkName: "users-edit" */ '@/pages/employees/EditUserPage.vue'
      )
  },
  {
    path: '/employees/create',
    name: 'employees-create',
    component: () =>
      import(
        /* webpackChunkName: "users-create" */ '@/pages/employees/CreateUser.vue'
      )
  },

]
