export default [{
  path: '/services',
  redirect: 'services-list'
}, {
  path: '/sections/list',
  name: 'sections-list',
  component: () => import(/* webpackChunkName: "users-list" */ '@/pages/services/sections.vue')
}, {
  path: '/categories/list',
  name: 'categories-list',
  component: () => import(/* webpackChunkName: "users-edit" */ '@/pages/services/categories.vue')
}, {
  path: '/services/list',
  name: 'services-list',
  component: () => import(/* webpackChunkName: "users-create" */ '@/pages/services/services.vue')
}
]
