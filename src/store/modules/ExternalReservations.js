/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  ExternalReservations: [],
  ExternalReservation: {},
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getExternalReservationsList: (state) => state.ExternalReservations,
  getExternalReservation: (state) => state.ExternalReservation,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setExternalReservationsList(state, payload) {
    state.ExternalReservations = payload;
  },
  setExternalReservation(state, payload) {
    state.ExternalReservation = payload;
  },
};
const actions = {
  fetchExternalReservations({ state, commit }, query) {
    let search = query.search ? `?SearchString=${query.search}` : "";
    return axios
      .get(`/api/v1/admin/external-reservations${search}`, { params: state.filter })
      .then((response) => {
        commit("setExternalReservationsList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  fetchExternalReservation({ commit }, id) {
    return axios.get(`/api/v1/admin/external-reservations/${id}`).then((response) => {
      commit("setExternalReservation", response.data.model);
      return response;
    });
  },
  cancelExternalReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/external-reservations/cancel`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
  completeExternalReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/external-reservations/complete`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
};

export default {
  namespaced: true,
  name: "ExternalReservations",
  state,
  getters,
  actions,
  mutations,
};
