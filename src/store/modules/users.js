import axios from "../../axios";
import swal from "sweetalert";

const state = {
  filters: { PageNumber: 1 },
  totalPages: 0,
  Providers: [],
  provider:{}
};

const getters = {
  getLastPage: (state) => state.last_page,
  getfilters: (state) => state.filters,
  getProvidersList: (state) => state.Providers,
  getProvider:(state)=>state.provider,
};
const mutations = {
  setTotalPages(state, payload) {
    state.totalPages = payload;
  },
  setfilters(state, filters) {
    for (const key in filters) {
      if (filters[key] == "") {
        filters[key] = undefined;
      }
    }
    state.filters = filters;
    console.log(state.filters)
  },
  SET_PROVIDERS_LIST(state, payload) {
    state.Providers = payload;
  },
  SET_PROVIDER(state,payload){
    state.provider = payload
  }
};
const actions = {
  fetchBeautyCenters({ state, commit } ) {
    return axios
      .get(`/api/v1/admin/provider/list?Type=1`, { params: state.filters })
      .then((response) => {
        commit("SET_PROVIDERS_LIST", response.data.model);
        commit("setTotalPages", response.data.totalPages);
        return response;
      });
  },
  fetchBeautyExperts({ state, commit } ) {
    return axios
      .get(`/api/v1/admin/provider/list?Type=2`, { params: state.filters })
      .then((response) => {
        commit("SET_PROVIDERS_LIST", response.data.model);
        commit("setTotalPages", response.data.totalPages);
        return response;
      });
  },
  fetchSingleProvider({ commit }, id) {
    return axios
      .get(`/api/v1/admin/provider/single/${id}`)
      .then((response) => {
        commit("SET_PROVIDER", response.data.model);
        return response;
      });
  },
  publishProvider({ commit }, item) {

    return axios
      .post(`/api/v1/admin/provider/publish/${item.id}?IsPublished=${item.IsPublished}`)
      .then((response) => {
        swal('Success ! ...', response.data.message, 'success');
        return response;
      });
  },
};

export default {
  namespaced: true,
  name: "users",
  state,
  getters,
  actions,
  mutations,
};
