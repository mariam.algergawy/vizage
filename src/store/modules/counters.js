/* eslint-disable */
import axios from "../../axios";

const state = {
  counters: [],
};

const getters = {
  getCountersList: (state) => state.counters,
};
const mutations = {
  setCountersList(state, payload) {
    state.counters = payload;
  },
};
const actions = {
  fetchCounters({ commit }, scope) {
    return axios
      .get(`/api/v1/admin/provider/counters?scope=${scope}`)
      .then((response) => {
        commit("setCountersList", response.data.model);
        return response;
      });
  },
};

export default {
  namespaced: true,
  name: "counters",
  state,
  getters,
  actions,
  mutations,
};
