/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  QuickReservations: [],
  QuickReservation: {},
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getQuickReservationsList: (state) => state.QuickReservations,
  getQuickReservation: (state) => state.QuickReservation,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setQuickReservationsList(state, payload) {
    state.QuickReservations = payload;
  },
  setQuickReservation(state, payload) {
    state.QuickReservation = payload;
  },
};
const actions = {
  fetchQuickReservations({ state, commit }, query) {
    let search = query.search ? `?SearchString=${query.search}` : "";
    return axios
      .get(`/api/v1/admin/Quick-reservations${search}`, { params: state.filter })
      .then((response) => {
        commit("setQuickReservationsList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  fetchQuickReservation({ commit }, id) {
    return axios.get(`/api/v1/admin/Quick-reservations/${id}`).then((response) => {
      commit("setQuickReservation", response.data.model);
      return response;
    });
  },
  cancelQuickReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/Quick-reservations/cancel`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
  completeQuickReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/Quick-reservations/complete`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
};

export default {
  namespaced: true,
  name: "QuickReservations",
  state,
  getters,
  actions,
  mutations,
};
