/* eslint-disable */
import axios from "../../axios";

const state = {
  Services: [],
  Offers: [],
  Packages: [],
};

const getters = {
  getServiceList: (state) => state.Services,
  getOffers: (state) => state.Offers,
  getPackages: (state) => state.Packages,
};
const mutations = {
  setServiceList(state, payload) {
    state.Services = payload;
  },
  setOffers(state, payload) {
    state.Offers = payload;
  },
  setPackages(state, payload) {
    state.Packages = payload;
  }
};
const actions = {
  fetchServices({  commit }, id) {
    return axios
      .get(`/api/v1/provider-services/by-id/${id}`)
      .then((response) => {
        commit("setServiceList", response.data.model);
        return response;
      });
  },
  fetchOffers({  commit }, id) {
    return axios
      .get(`/api/v1/offers/provider/${id}`)
      .then((response) => {
        commit("setOffers", response.data.model);
        return response;
      });
  }
  ,
  fetchPackages({  commit }, id) {
    return axios
      .get(`/api/v1/packages/provider/${id}`)
      .then((response) => {
        commit("setPackages", response.data.model);
        return response;
      });
  }
};

export default {
  namespaced: true,
  name: "accountInformation",
  state,
  getters,
  actions,
  mutations,
};
