import axios from "../../axios";
import swal from "sweetalert";

const state = {
  notificationsent: {},
  Countries: [],
};

const getters = {
  getNotificationsent(state) {
    return state.notificationsent;
  },
  getCountries(state) {
    return state.Countries;
  }
};
const mutations = {
  sendNotification: (state, notificationsent) =>
    (state.notificationsent = notificationsent),
  setCountries: (state, Countries) => (state.Countries = Countries)
};
const actions = {
  fetchCountries({ commit }) {
    return axios
      .get("/api/v1/countries")
      .then((response) => {
        commit("setCountries", response.data.model);
        return response;
      });
  },
  sendNotification({ commit }, notification) {
    let formData = new FormData();
    formData.append("CountryCode", notification.CountryCode);
    formData.append("TitleEn", notification.TitleEn);
    formData.append("TitleAr", notification.TitleAr);
    formData.append("MessageEn", notification.MessageEn);
    formData.append("MessageAr", notification.MessageAr);
    return axios
      .post(`/api/v1/admin/notification/send `, formData)
      .then((response) => {
        swal('Success ! ...', response.data.message, 'success');
        commit("sendNotification", response.data);
        return response;
      });
  },
};

export default {
  namespaced: true,
  name: "sendNotification",
  state,
  getters,
  actions,
  mutations,
};
