/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";
import i18n from "../../plugins/vue-i18n";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  Services: [],
  Service: {},
  ServiceStatus: [],
  categories: [],
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getServiceList: (state) => state.Services,
  getService: (state) => state.Service,
  getServiceStatus: (state) => state.ServiceStatus,
  getCategories: (state) => state.categories,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setServiceList(state, payload) {
    state.Services = payload;
  },
  setService(state, payload) {
    state.Service = payload;
  },
  setServiceStatus(state, payload) {
    state.ServiceStatus = payload;
  },
  setCategories(state, payload) {
    state.categories = payload;
  }
};
const actions = {
  fetchServices({ state, commit } ,query) {
    let search = query.search ? query.search : "";
    let filter = `&CategoryID=${query.filter}`;
    return axios
      .get(`/api/v1/admin/service/list?SearchString=${search}${filter}`, { params: state.filter })
      .then((response) => {
        commit("setServiceList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  fetchCategories({ commit } , query) {
    let filter = query.section ? `?SectionID=${query.section}` : "";
    return axios
      .get(`/api/v1/admin/category/list${filter}`, { params: { PageSize: 10000 } })
      .then((response) => {
        commit('setCategories', response.data.model)
        return response
      })
  },
  updateService({ commit }, item) {
    let formData = new FormData();
    item.NameAr ? formData.append("NameAr", item.NameAr) : null;
    item.NameEn ? formData.append("NameEn", item.NameEn) : null;
    item.DisplayOrder ? formData.append("DisplayOrder", item.DisplayOrder) : null;
    item.CategoryID ? formData.append("CategoryID", item.CategoryID) : null;
    item.PriceFrom ? formData.append("PriceFrom", item.PriceFrom) : null;
    item.PriceTo ? formData.append("PriceTo", item.PriceTo) : null;
    item.HasFixedPrice ? formData.append("HasFixedPrice", item.HasFixedPrice) : null;
    item.Duration ? formData.append("Duration", item.Duration) : null;


    return axios.put(`/api/v1/admin/service`, formData).then((response) => {
      commit("setService", response.data.model);
      swal(
        i18n.t("Service.ServiceUpdatedSuccessfully") ,
        response.data.message,
        "success");
      return response;
    });
  },
  postService({ commit }, item) {
    let formData = new FormData();
    formData.append("Id", item.id);
    formData.append("NameAr", item.NameAr);
    formData.append("NameEn", item.NameEn);
    formData.append("DisplayOrder", item.DisplayOrder);
    formData.append("CategoryID", item.CategoryID);
    formData.append("PriceFrom", item.PriceFrom);
    formData.append("PriceTo", item.PriceTo);
    formData.append("HasFixedPrice", item.HasFixedPrice);
    formData.append("Duration", item.Duration);
    formData.append("Indoor", item.Indoor);
    formData.append("Outdoor", item.Outdoor);
    return axios
      .post(`/api/v1/admin/service`, formData)
      .then((response) => {
        commit("setService", response.data.model);
        swal(
          i18n.t("Service.ServiceCreatedSuccessfully") ,
          response.data.message,
         "success");
        return response;
      }).catch((err) => {

        swal(
          i18n.t("common.error"),
          i18n.t("common.informationIsIncorrect"),
          "error"
        );
    });
  },
  changeServiceStatus({ commit }, item) {

    let id = item.id;
    let status = item.status;
    return axios
      .put(`/api/v1/admin/service/status/${id}?IsActive=${status}`)
      .then((response) => {
        commit("setServiceStatus", response.data.model);
        return response;
      });
  },
  deleteService({ commit }, id) {
    return axios
      .delete(`/api/v1/admin/service/${id}`)
      .then((response) => {
        commit("setService", response.data.model);
        return response;
      });
  }
};

export default {
  namespaced: true,
  name: "Service",
  state,
  getters,
  actions,
  mutations,
};
