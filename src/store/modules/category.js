/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";
import i18n from "../../plugins/vue-i18n";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  Categories: [],
  Category: {},
  CategoryStatus: [],
  Sections: [],
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getCategoriesList: (state) => state.Categories,
  getCategory: (state) => state.Category,
  getCategoryStatus: (state) => state.CategoryStatus,
  getSections: (state) => state.Sections,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setCategoriesList(state, payload) {
    state.Categories = payload;
  },
  setCategory(state, payload) {
    state.Category = payload;
  },
  setCategoryStatus(state, payload) {
    state.CategoryStatus = payload;
  },
  setSections(state, payload) {
    state.Sections = payload;
  }
};
const actions = {
  fetchCategorys({ state, commit } ,query) {
    let search = query.search ? query.search : "";
    let filter = query.filter ? `&SectionID=${query.filter}` : "";
    return axios
      .get(`/api/v1/admin/category/list?SearchString=${search}${filter}`, { params: state.filter })
      .then((response) => {
        commit("setCategoriesList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  fetchSections({ commit }) {
    return axios
      .get(`/api/v1/admin/section/list`, { params: { PageSize: 10000 } })
      .then((response) => {
        commit('setSections', response.data.model)
        return response
      })
  },
  updateCategory({ commit }, item) {
    let formData = new FormData();
    item.id ? formData.append("Id", item.id) : null;
    item.NameAr ? formData.append("NameAr", item.NameAr) : null;
    item.NameEn ? formData.append("NameEn", item.NameEn) : null;
    item.DisplayOrder ? formData.append("DisplayOrder", item.DisplayOrder) : null;
    item.SectionID ? formData.append("SectionID", item.SectionID) : null;

    return axios.put(`/api/v1/admin/category`, formData).then((response) => {
      commit("setCategory", response.data.model);
      swal(
        i18n.t("Category.CategoryUpdatedSuccessfully") ,
        response.data.message,
        "success");
      return response;
    });
  },
  postCategory({ commit }, item) {
    let formData = new FormData();
    formData.append("Id", item.id);
    formData.append("NameAr", item.NameAr);
    formData.append("NameEn", item.NameEn);
    formData.append("DisplayOrder", item.DisplayOrder);
    formData.append("SectionID", item.SectionID);
    return axios
      .post(`/api/v1/admin/category`, formData)
      .then((response) => {
        commit("setCategory", response.data.model);
        swal(
          i18n.t("Category.CategoryCreatedSuccessfully") ,
          response.data.message,
         "success");
        return response;
      }).catch((err) => {

        swal(
          i18n.t("common.error"),
          i18n.t("common.informationIsIncorrect"),
          "error"
        );
    });
  },
  changeCategoryStatus({ commit }, item) {

    let id = item.id;
    let status = item.status;
    return axios
      .put(`/api/v1/admin/category/status/${id}?IsActive=${status}`)
      .then((response) => {
        commit("setCategoryStatus", response.data.model);
        return response;
      });
  },
  deleteCategory({ commit }, id) {
    return axios
      .delete(`/api/v1/admin/category/${id}`)
      .then((response) => {
        commit("setCategory", response.data.model);
        return response;
      });
  }
};

export default {
  namespaced: true,
  name: "Category",
  state,
  getters,
  actions,
  mutations,
};
