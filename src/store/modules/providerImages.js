import axios from "../../axios";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  Images: [],
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getProviderImagesList: (state) => state.Images,
};
const mutations = {
  setTotalPages(state, payload) {
    state.last_page = payload;
  },

  setFilter(state, payload) {
    state.filter = payload;
  },
  SET_PROVIDERS_IMAGES_LIST(state, payload) {
    state.Images = payload;
  },

};
const actions = {
  fetchRecords({ state, commit },query) {
    let search = query.search  ? `?SearchString=${query.search}` : "";
    return axios
      .get(`/api/v1/admin/provider/images/list${search}`, { params: state.filter })
      .then((response) => {
        commit("SET_PROVIDERS_IMAGES_LIST", response.data.model);
        commit("setTotalPages", response.data.totalPages);
        return response;
      });
  },
  //approve or disapprove image
  approveOrDisapproveImage({ commit }, item) {
    let formData = new FormData();
    formData.append("id", item.id);
    formData.append("isActive", item.isActive);
    item.isActive == false ? formData.append("RejectReason", item.RejectReason) : null;
    return axios
      .post(`/api/v1/admin/provider/images/approve`, formData)

  }
};

export default {
  namespaced: true,
  name: "providerImages",
  state,
  getters,
  actions,
  mutations,
};
