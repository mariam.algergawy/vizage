/* eslint-disable */
import axios from '../../axios'
import swal from 'sweetalert'
import i18n from "../../plugins/vue-i18n";
const state = {
  filter: { page: 1 },
  last_page: 0,
  Moderators: [],
  Moderator: {},
  permissions: [],
  moderatorStatus: []
}

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getModeratorsList: (state) => state.Moderators,
  getModerator: (state) => state.Moderator,
  getPermissionsList: (state) => state.permissions,
  getModeratorStatus: (state) => state.moderatorStatus
}
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload
  },
  setFilter(state, payload) {
    state.filter = payload
  },
  setModeratorsList(state, payload) {
    state.Moderators = payload
  },
  setModerator(state, payload) {
    state.Moderator = payload
  },
  setPermissionsList(state, payload) {
    state.permissions = payload
  },
  setModeratorStatus(state, payload) {
    state.moderatorStatus = payload
  }
}
const actions = {
  fetchModerators({ state, commit }) {
    return axios
      .get(`/api/v1/admin/user/list`, { params: state.filter })
      .then((response) => {
        commit('setModeratorsList', response.data.model)
        commit('setLastPage', response.data.totalPages)
        return response
      })
  },
  fetchModerator({ commit }, id) {
    return axios.get(`/api/v1/admin/user/single/${id}`).then((response) => {
      commit('setModerator', response.data.model)
      return response
    })
  },
  postModerator({ commit }, item) {
    let formData = new FormData()
    formData.append('Name', item.name)
    formData.append('PhoneNumber', item.phone)
    formData.append('Email', item.email)
    formData.append('Password', item.password)

    return axios
      .post(`/api/v1/admin/user`, formData)
      .then((response) => {
        commit('setModerator', response.data.model)
        swal(
          i18n.t("users.employeeCreatedSuccessfully") ,
          response.data.message,
          "success");
        return response
      })
      .catch((error) => {
        console.log(error.response.data.errors.messages[0])
        swal('Error ! ...', error.response.data.errors.messages[0], 'error')
      })
  },
  ubdateModerator({ commit }, item) {
    let formData = new FormData()
    formData.append('Id', item.UserId)
    formData.append('Name', item.name)
    formData.append('PhoneNumber', item.phone)
    formData.append('Email', item.email)
    formData.append('Password', item.password)
    formData.append('IsActive', true)
    formData.append('IsDeleted', false)

    return axios
      .put(`/api/v1/admin/user`, formData)
      .then((response) => {
        commit('setModerator', response.data.model);
        swal(
          i18n.t("users.employeeUpdatedSuccessfully") ,
          response.data.message,
          "success");
        return response
      })
  },
  changeModeratorStatus({ commit }, item) {

    return axios
      .put(`/api/v1/admin/user/status/${item.UserId}?IsActive=${item.IsActive}`)
      .then((response) => {
        commit('setModeratorStatus', response.data.model)
        return response
      })
  }
}

export default {
  namespaced: true,
  name: 'Moderators',
  state,
  getters,
  actions,
  mutations
}
