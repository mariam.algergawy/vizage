/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";
import i18n from "../../plugins/vue-i18n";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  Sections: [],
  Section: {},
  SectionStatus: [],
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getSectionList: (state) => state.Sections,
  getSection: (state) => state.Section,
  getSectionStatus: (state) => state.SectionStatus,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setSectionList(state, payload) {
    state.Sections = payload;
  },
  setSection(state, payload) {
    state.Section = payload;
  },
  setSectionStatus(state, payload) {
    state.SectionStatus = payload;
  },
};
const actions = {
  fetchSections({ state, commit } ,query) {
    let search = query.search ? `?SearchString=${query.search}` : "";
    return axios
      .get(`/api/v1/admin/section/list${search}`, { params: state.filter })
      .then((response) => {
        commit("setSectionList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  updateSection({ commit }, item) {
    let formData = new FormData();
    item.id ? formData.append("Id", item.id) : null;
    item.NameAr ? formData.append("NameAr", item.NameAr) : null;
    item.NameEn ? formData.append("NameEn", item.NameEn) : null;
    item.DisplayOrder ? formData.append("DisplayOrder", item.DisplayOrder) : null;
    item.IconFile ? formData.append("IconFile", item.IconFile) : null;

    return axios.put(`/api/v1/admin/section`, formData).then((response) => {
      commit("setSection", response.data.model);
      swal(
        i18n.t("Section.SectionUpdatedSuccessfully") ,
        response.data.message,
        "success");
      return response;
    });
  },
  postSection({ commit }, item) {
    let formData = new FormData();
    formData.append("Id", item.id);
    formData.append("NameAr", item.NameAr);
    formData.append("NameEn", item.NameEn);
    formData.append("DisplayOrder", item.DisplayOrder);
    formData.append("IconFile", item.IconFile);
    return axios
      .post(`/api/v1/admin/section`, formData)
      .then((response) => {
        commit("setSection", response.data.model);
        swal(
          i18n.t("Section.SectionCreatedSuccessfully") ,
          response.data.message,
         "success");
        return response;
      }).catch((err) => {

        swal(
          i18n.t("common.error"),
          i18n.t("common.informationIsIncorrect"),
          "error"
        );
    });
  },
  changeSectionStatus({ commit }, item) {

    let id = item.id;
    let status = item.status;
    return axios
      .put(`/api/v1/admin/section/status/${id}?IsActive=${status}`)
      .then((response) => {
        commit("setSectionStatus", response.data.model);
        return response;
      });
  },
  deleteSection({ commit }, id) {
    return axios
      .delete(`/api/v1/admin/section/${id}`)
      .then((response) => {
        commit("setSection", response.data.model);
        return response;
      });
  }
};

export default {
  namespaced: true,
  name: "Sections",
  state,
  getters,
  actions,
  mutations,
};
