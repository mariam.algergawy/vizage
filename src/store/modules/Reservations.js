/* eslint-disable */
import axios from "../../axios";
import swal from "sweetalert";

const state = {
  filter: { PageNumber: 1 },
  last_page: 0,
  Reservations: [],
  Reservation: {},
};

const getters = {
  getLastPage: (state) => state.last_page,
  getFilter: (state) => state.filter,
  getReservationsList: (state) => state.Reservations,
  getReservation: (state) => state.Reservation,
};
const mutations = {
  setLastPage(state, payload) {
    state.last_page = payload;
  },
  setFilter(state, payload) {
    state.filter = payload;
  },
  setReservationsList(state, payload) {
    state.Reservations = payload;
  },
  setReservation(state, payload) {
    state.Reservation = payload;
  },
};
const actions = {
  fetchReservations({ state, commit }, query) {
    let search = query.search ? `?SearchString=${query.search}` : "";
    return axios
      .get(`/api/v1/admin/reservations${search}`, { params: state.filter })
      .then((response) => {
        commit("setReservationsList", response.data.model);
        commit("setLastPage", response.data.totalPages);
        return response;
      });
  },
  fetchReservation({ commit }, id) {
    return axios.get(`/api/v1/admin/reservations/${id}`).then((response) => {
      commit("setReservation", response.data.model);
      return response;
    });
  },
  cancelReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/reservations/cancel`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
  completeReservation({ commit }, item) {
    return axios.put(`/api/v1/admin/reservations/complete`, item).then((response) => {
      swal('Success ! ...', response.data.message, 'success');
      return response;
    });
  },
};

export default {
  namespaced: true,
  name: "Reservations",
  state,
  getters,
  actions,
  mutations,
};
