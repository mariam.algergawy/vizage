import axios from '../axios'
import swal from 'sweetalert'
export default {
  state() {
    return {
      token: null || localStorage.getItem('token'),
      didLogOut: false,
      error: null
    }
  },
  mutations: {
    setAdmin(state, payload) {
      state.token = payload.idToken
      state.didLogOut = false
    },
    setError(state, payload) {
      state.error = payload
    }
  },
  actions: {
    async login({ commit }, { email, password  }) {
      const data = new FormData()

      data.append('Email', email)
      data.append('Password', password)
      data.append('Role', 'PROVIDER')
      data.append('Type', 'email')

      const response = await axios
        .post('/api/v1/auth/pe/request-signing-in', data)
        .catch((error) => {
          swal('Error ! ...', error.response.data.message, 'error')
        })

      if (response.status === 401) {
        alert('Invalid username or password')
      }
      if (response.status === 200) {
        const dataResponse = response.data.model.tokens

        localStorage.setItem('token', dataResponse.accessToken)
        localStorage.setItem('lang', 'ar-SA')
        commit('setAdmin', {
          idToken: dataResponse.accessToken
        })

        commit('setError', null)
        // localStorage.setItem("token", response.data.idToken);
      } else {
        commit('setError', response.data.message)
      }
    },

    async autoLogin({ commit }) {
      const token = localStorage.getItem('token')

      if (!token) {
        return
      } else {
        commit('setAdmin', {
          idToken: token,
          didLogOut: false
        })
      }
    }

    // async logout({ commit }) {

    // },
  },
  getters: {
    isAuthenticated(state) {
      return state.token !== null
    },
    getErrorMessage(state) {
      return state.error
    }
  }
}
