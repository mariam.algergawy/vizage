import Vue from 'vue'
import Vuex from 'vuex'

// Global vuex
import AppModule from './app'
import AdminLogin from './AdminLogin'
// Example Apps
import BoardModule from '../apps/board/store'
import EmailModule from '../apps/email/store'
import TodoModule from '../apps/todo/store'

Vue.use(Vuex)
const modules = {}
const context = require.context('@/store/modules', true)

context
  .keys()
  .map(context)
  .map((m) => m.default)
  .forEach((resource) => {
    modules[resource.name] = resource
  })
/**
 * Main Vuex Store
 */

const store = new Vuex.Store({
  modules: {
    app: AppModule,
    'board-app': BoardModule,
    'email-app': EmailModule,
    'todo-app': TodoModule,
    ...modules
  },

  ...AdminLogin
})

export default store
