import store from './store'
import Axios from 'axios'

const axios = Axios.create({
  // baseURL: 'https://api.mvappsi.xyz/api/v1',
  baseURL: 'https://vizage-dev-legacy.azurewebsites.net/',
  headers: {
    // content
    'Content-Type': 'application/json',
    //  lang
    'Accept-Language': localStorage.getItem('lang')
    //  Authorization
  }
})


axios.interceptors.request.use((config) => {
  config.headers.Authorization = store.state.token
    ? 'Bearer ' + store.state.token
    : ''

  return config
})
export default axios
