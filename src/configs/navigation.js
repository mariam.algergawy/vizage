import menuUI from "./menus/ui.menu";
import menuApps from "./menus/apps.menu";
import menuPages from "./menus/pages.menu";

export default {
  // main navigation - side menu
  menu: [
    {
      text: "",
      key: "",
      items: [
        {
          icon: "mdi-view-dashboard-outline",
          key: "menu.dashboard",
          text: "Dashboard",
          link: "/dashboard/analytics",
        },
      ],
    },
    //  {
    //   text: 'Apps',
    //   items: menuApps
    // },

    {
      text: "",
      key: "",
      items: [
        {
          icon: 'mdi-account-multiple-outline',
          key: 'menu.employees',
          text: 'employees',
          link: '/employees/list'
        },
        {
          icon: "mdi mdi-account",
          key: "menu.serviceProvider",
          text: "Beauty experts",
          items: [
            {
              text: "Reservations",
              key: "menu.BeautyCenters",
              link: "/BeautyCenters/list",
            },

            {
              text: "Reservations",
              key: "menu.BeautyExperts",
              link: "/BeautyExperts/list",
            },

          ],
        },
      ],
    },
    {
      text: "",
      key: "",
      items: [
        {
          icon: "mdi mdi-account",
          key: "menu.services",
          text: "services",
          items: [
            {
              text: "sections",
              key: "menu.sections",
              link: "/sections/list",
            },
            {
              text: "categories",
              key: "menu.categories",
              link: "/categories/list",
            },
            {
              text: "services",
              key: "menu.services",
              link: "/services/list",
            },
          ],
        },
      ],
    },
    {
      text: "",
      key: "",
      items: [

        {
          icon: "mdi-cash-usd-outline",
          key: "menu.Reservations",
          text: "Reservations",
          link: "/utility/maintenance",
          items: [
            { text: "Reservations", key: "menu.Reservations" },
            { text: "Urgent Reservations", key: "menu.UrgentReservations" },
            { text: "external Reservations", key: "menu.externalReservations" },
          ],
        },
        {
          icon: "mdi mdi-camera-image",
          key: "menu.ManagePhotos",
          text: "Manage photos",
          link: "/providerImages",
        },

        {
          icon: "mdi mdi-doctor",
          key: "menu.ManageOrderSuppliersService",
          text: "Manage order suppliers",
          link: "/utility/maintenance",
        },
        {
          icon: "mdi mdi-bell-circle",
          key: "menu.ManageNotifications",
          text: "Manage notifications",
          link: "/push/notifications",
        },
        {
          icon: "mdi mdi-wallet-outline",
          key: "menu.ManageWallet",
          text: "Manage wallet",
          link: "/landing",
        },
        {
          icon: "mdi mdi-cards",
          key: "menu.ManagePoints",
          text: "Manage points",
          link: "/landing",
        },
        {
          icon: "mdi mdi-google-spreadsheet",
          key: "menu.ManageReports",
          text: "Manage reports",
          link: "/landing",
        },
        {
          icon: "mdi mdi-offer",
          key: "menu.ManageCoupons",
          text: "Manage promotions ",
          link: "/landing",
        },
        {
          icon: "mdi mdi-cards-heart",
          key: "menu.ManageRating",
          text: "Manage rating ",
          link: "/landing",
        },
        {
          icon: "mdi mdi-cellphone-sound",
          key: "menu.ManageAdvertising",
          text: "Manage Advertising ",
          link: "/landing",
        },
      ],
    },
    // {
    //   text: 'UI - Theme Preview',
    //   items: menuUI
    // },
    // {
    //   text: 'Pages',
    //   key: 'menu.pages',
    //   items: menuPages
    // },
    // {
    //   text: "",
    //   key: "",
    //   items: [
    //     {
    //       icon: "mdi mdi-account",
    //       key: "menu.ManageUsersRoles",
    //       text: "Manage Users Roles",
    //       link: "/landing",
    //     },
    //     {
    //       icon: "mdi mdi-video-input-component",
    //       key: "menu.ManageSettings",
    //       text: "Settings",
    //       link: "/landing/pricing",
    //       items: [
    //         { text: "Manage Services", key: "menu.ManageServices" },
    //         {
    //           text: "Manage Execution Services Places",
    //           key: "menu.ManageUsersRoles",
    //         },
    //         {
    //           text: "Manage Suppliers Featutres",
    //           key: "menu.ManageExecutionServicesPlaces",
    //         },
    //         {
    //           text: "Manage Suppliers Notifications",
    //           key: "menu.ManageSuppliersNotifications",
    //         },
    //         {
    //           text: "Manage Suppliers schedule",
    //           key: "menu.ManageSuppliersSchedule",
    //         },
    //         {
    //           text: "Manage Countries Names",
    //           key: "menu.ManageCountriesNames",
    //         },
    //         { text: "Manage Area Names", key: "menu.ManageAreaNames" },
    //         { text: "Manage Cities Names", key: "menu.ManageCitiesNames" },
    //         { text: "Manage Streets Names", key: "menu.ManageStreetsNames" },
    //         {
    //           text: "Manage User Categories",
    //           key: "menu.ManageUserCategories",
    //         },
    //       ],
    //     },
    //     {
    //       icon: "mdi mdi-credit-card-search",
    //       key: "menu.ManageTicketsSettings",
    //       text: "Manage Tickets Settings",
    //       link: "/landing",
    //     },
    //   ],
    // },
    {
      text: "",
      key: "",
      items: [
        {
          icon: "mdi-update",
          key: "menu.Updates",
          text: "updates",
          link: "/blank",
          items: [
            {
              text: "Update TermsAndComdition sUser",
              key: "menu.UpdateTermsAndConditionsUser",
            },
            {
              text: "Update Terms And Comditions Suppliers",
              key: "menu.UpdateTermsAndConditionsSuppliers",
            },
            { text: "Update Privacy User", key: "menu.UpdatePrivacyUser" },
            {
              text: "Update Privacy Suppliers",
              key: "menu.UpdatePrivacySuppliers",
            },
            {
              text: "Update Application User",
              key: "menu.UpdateApplicationUser",
            },
            {
              text: "Update Application Suppliers",
              key: "menu.UpdateApplicationSuppliers",
            },
          ],
        },
      ],
    },
  ],
};
